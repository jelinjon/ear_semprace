package ear.kos2;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

//DataSourceAutoConfiguration.class
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class, DataSourceAutoConfiguration.class})   // Temporarily disable security
public class KosApplication {

    public static void main(String[] args) {
        SpringApplication.run(KosApplication.class, args);
    }
}

