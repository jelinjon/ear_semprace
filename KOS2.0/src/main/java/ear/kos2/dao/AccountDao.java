package ear.kos2.dao;

import ear.kos2.model.Account;
import jakarta.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDao extends BaseDao<Account> {
    protected AccountDao() {
        super(Account.class);
    }

    public boolean accountExists(String username, String password) {
        String select = "SELECT ua FROM Account ua WHERE ua.username=:username and ua.password=:password";

        Query query = em.createQuery(select);
        query.setParameter("username", username);
        query.setParameter("password", password);
        Account a;
        a = (Account) query.getSingleResult();

        return a != null;
    }
}
