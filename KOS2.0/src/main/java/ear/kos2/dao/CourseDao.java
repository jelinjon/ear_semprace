package ear.kos2.dao;

import ear.kos2.model.Course;
import org.springframework.stereotype.Repository;

@Repository
public class CourseDao extends BaseDao<Course> {
    protected CourseDao() {
        super(Course.class);
    }
}
