package ear.kos2.dao;

import ear.kos2.model.Location;
import org.springframework.stereotype.Repository;

@Repository
public class LocationDao extends BaseDao<Location> {
    protected LocationDao() {
        super(Location.class);
    }
}
