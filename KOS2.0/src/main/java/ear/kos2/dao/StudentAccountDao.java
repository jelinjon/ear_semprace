package ear.kos2.dao;

import ear.kos2.model.StudentAccount;
import org.springframework.stereotype.Repository;

@Repository
public class StudentAccountDao extends BaseDao<StudentAccount> {
    protected StudentAccountDao() {
        super(StudentAccount.class);
    }
}
