package ear.kos2.dao;

import ear.kos2.model.TeacherAccount;
import org.springframework.stereotype.Repository;

@Repository
public class TeacherAccountDao extends BaseDao<TeacherAccount> {
    protected TeacherAccountDao() {
        super(TeacherAccount.class);
    }
}
