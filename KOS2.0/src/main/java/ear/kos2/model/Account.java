package ear.kos2.model;

import jakarta.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Account extends AbstractEntity {
    @Basic
    @Column
    protected String username;
    @Basic
    @Column
    protected String password;
    @Basic
    @Column
    protected String firstName;
    @Basic
    @Column
    protected String lastName;
    @Basic
    @Column
    protected Boolean online;
    @Enumerated
    @Column
    protected Role role;
    @OneToOne
    protected Schedule schedule;

    public Account() {
    }

    public List<Course> searchCourses(String name) {
        return null;
    }

    public List<Course> showMyCourses() {
        return null;
    }

    public Schedule getSchedule() {
        return this.schedule;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public Boolean getOnline() {
        return online;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private enum Role {
        STUDENT(0),
        TEACHER(1);

        private final int code;

        Role(int i) {
            this.code = i;
        }
    }
}
