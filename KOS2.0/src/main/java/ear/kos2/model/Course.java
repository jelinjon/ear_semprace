package ear.kos2.model;

import jakarta.persistence.*;
import java.util.List;

@Entity
public class Course extends AbstractEntity {
    @Basic
    @Column
    private String name;
    @Basic
    @Column
    private Integer credits;
    @Basic
    @Column
    private Integer capacity;
    @ManyToMany
    @JoinTable
    private List<TeacherAccount> teachers;
    @ManyToMany
    @JoinTable
    private List<StudentAccount> students;
    @ManyToMany
    @JoinTable
    private List<SClass> classes;

    public Course() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<TeacherAccount> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<TeacherAccount> teachers) {
        this.teachers = teachers;
    }

    public List<StudentAccount> getStudents() {
        return students;
    }

    public void setStudents(List<StudentAccount> students) {
        this.students = students;
    }

    public List<SClass> getClasses() {
        return classes;
    }

    public void setClasses(List<SClass> classes) {
        this.classes = classes;
    }
}
