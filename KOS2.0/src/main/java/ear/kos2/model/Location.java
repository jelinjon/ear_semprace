package ear.kos2.model;

import jakarta.persistence.*;

@Entity
public class Location extends AbstractEntity {
    @Basic
    @Column
    private String street;
    @Basic
    @Column
    private String building;
    @Basic
    @Column
    private String room;

    public void update(String street, String buillding, String room) {
        if (street != null) {
            this.street = street;
        }
        if (building != null) {
            this.building = buillding;
        }
        if (room != null) {
            this.room = room;
        }
    }
}
