package ear.kos2.model;

import jakarta.persistence.*;
import java.util.List;
import java.time.LocalTime;

@Entity
public class SClass extends AbstractEntity {
    @Enumerated
    @Column
    public Day day;
    @Basic
    @Column
    public LocalTime start;
    @Basic
    @Column
    public LocalTime end;
    @Basic
    @Column
    private String number;
    @Enumerated
    @Column
    private ClassType type;
    @Basic
    @Column
    private Integer capacity;
    @OneToOne
    private Course course;
    @ManyToMany
    @JoinTable
    private List<TeacherAccount> teachers;
    @ManyToOne
    @JoinColumn
    private Location location;

    public SClass(){
        this.location = new Location();
    }

    public void setTime(Day day, LocalTime start, LocalTime end) {
        this.day = day;
        this.start = start;
        this.end = end;
    }

    /**
     * enum for assigning lecture or exercise to a certain class <br>
     * code == 0 if LECTURE <br>
     * code == 1 if EXERCISE
     */
    private enum ClassType{
        LECTURE (0),
        EXERXCISE (1);

        private final int code;
        ClassType(int i) {
            this.code = i;
        }
    }
}
