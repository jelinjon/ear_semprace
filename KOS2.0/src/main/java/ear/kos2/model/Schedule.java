package ear.kos2.model;

import jakarta.persistence.*;
import java.util.*;

@Entity
public class Schedule extends AbstractEntity {
    @OneToOne
    private Account owner;
    @Basic
    @Column
    private String semester;
    @ManyToMany
    @JoinTable
    private List<SClass> classes;

    public Schedule() {
    }

    public Schedule(Account owner) {
        this.owner = owner;
    }

    public void addClass(SClass c) {
        this.classes.add(c);
    }

    public void removeClass(SClass c) {
        classes.remove(c);
    }

    public List<SClass> getClasses() {
        return this.classes;
    }

    public void add(SClass sclass) {
        Objects.requireNonNull(sclass);

        if (classes == null) {
            classes = new ArrayList<>();
        }
        if (!classes.contains(sclass)) {
            classes.add(sclass);
        }
    }
}
