package ear.kos2.model;

import ear.kos2.exception.AccountException;
import jakarta.persistence.*;
import java.util.*;

@Entity
public class StudentAccount extends Account {
    @Basic
    @Column
    private String faculty;
    @Basic
    @Column
    private String program;
    @Basic
    @Column
    private String specialization;
    @Basic
    @Column
    private Integer credits;
    @ManyToMany
    @JoinTable
    private List<Course> courses;

    public List<Course> getCourses() {
        return courses;
    }

    public void addToCourses(Course course){
        Objects.requireNonNull(course);

        if(courses == null){
            courses = new ArrayList<>();
            courses.add(course);
            return;
        }

        if(!courses.contains(course)){
            courses.add(course);
        }
    }

    public void removeFromCourses(Course course){
        Objects.requireNonNull(course);

        if(courses.contains(course)){
            courses.remove(course);
        }
        else {
            throw new AccountException("");
        }
    }

    public void addToSchedule(SClass sclass){
        Objects.requireNonNull(sclass);

        if(schedule == null){
            schedule = new Schedule(this);
            schedule.add(sclass);
            return;
        }

        schedule.add(sclass);
    }
}
