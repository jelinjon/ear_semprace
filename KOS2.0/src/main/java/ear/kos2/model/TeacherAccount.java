package ear.kos2.model;

import jakarta.persistence.*;
import java.util.List;

@Entity
public class TeacherAccount extends Account {
    @Basic
    @Column
    private String title;
    @ManyToOne
    @JoinColumn
    private final Location location;
    @ManyToMany
    @JoinTable
    private List<Course> courses;
    @ManyToMany
    @JoinTable
    private List<SClass> classes;

    public List<Course> getCourses() {
        return courses;
    }

    public TeacherAccount(){
        this.location = new Location();
    }

    public void ChangeLocation(String street, String building, String room){
        this.location.update(street, building, room);
    }
}
