package ear.kos2.service;

import ear.kos2.dao.AccountDao;
import ear.kos2.dao.SClassDao;
import ear.kos2.dao.CourseDao;
import ear.kos2.dao.StudentAccountDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.Course;
import ear.kos2.model.SClass;
import ear.kos2.model.StudentAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentAccountService extends AccountService{
    private final SClassDao classDao;
    private final StudentAccountDao studentAccountDao;

    private final StudentAccount studentAccount = new StudentAccount();

    @Autowired
    public StudentAccountService(AccountDao accountDao, CourseDao courseDao, SClassDao sClassDao, StudentAccountDao studentAccountDao) {
        super(accountDao, courseDao);
        this.classDao = sClassDao;
        this.studentAccountDao = studentAccountDao;
    }

    @Transactional
    public List<Course> showMyCourses(){
        return studentAccount.getCourses();
    }

    @Transactional
    public void addCourse(int id){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }
        studentAccount.addToCourses(course);
        studentAccountDao.update(studentAccount);
    }

    @Transactional
    public void removeCourse(int id){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }
        studentAccount.removeFromCourses(course);
        studentAccountDao.update(studentAccount);
    }

    @Transactional
    public void addToSchedule(int id){
        SClass sclass = classDao.find(id);
        if(sclass == null){
            throw new AccountException("Class with that id does not exist");
        }
        studentAccount.addToSchedule(sclass);
        studentAccountDao.update(studentAccount);
    }

    @Transactional
    public void editInSchedule(){

    }
}
