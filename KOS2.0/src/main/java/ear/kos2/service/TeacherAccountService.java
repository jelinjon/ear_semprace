package ear.kos2.service;

import ear.kos2.dao.AccountDao;
import ear.kos2.dao.CourseDao;
import ear.kos2.exception.AccountException;
import ear.kos2.model.Course;
import ear.kos2.model.TeacherAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TeacherAccountService extends AccountService {

    private final TeacherAccount teacherAccount = new TeacherAccount();


    @Autowired
    public TeacherAccountService(AccountDao accountDao, CourseDao courseDao) {
        super(accountDao, courseDao);
    }

    @Transactional
    public List<Course> showMyCourses(){
        return teacherAccount.getCourses();
    }

    @Transactional
    public void createCourse(){
        Course course = new Course();
        course.setName("name");
        course.setCapacity(10);
        // ...
        courseDao.persist(course);
    }

    @Transactional
    public void editCourse(int id, String name, int credits, int capacity){
        Course course = courseDao.find(id);
        if(course == null){
            throw new AccountException("Course with that id does not exist");
        }

        if(name != null){
            course.setName(name);
        }
        else{
            throw new AccountException("Name of course cannot be null");
        }
        if(credits > 0){
            course.setCredits(credits);
        }
        else {
            throw new AccountException("Number of credits has to exceed 0");
        }
        if(capacity > 0){
            course.setCapacity(capacity);
        }
        else {
            throw new AccountException("Capacity has to exceed 0");
        }

        courseDao.update(course);
    }
}
